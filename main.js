const base = document.querySelector(".base");
const container = document.querySelector(".container");

// button
const plus = document.querySelector(".plus");
const minus = document.querySelector(".minus");

// hover chaining
const targetHover = document.querySelectorAll("#hover")

// ************************** scroll direction
function onWheel(event){
    if (event.deltaY < 0) {
        base.style.transform = `scale(${scale += reScale})`
    }
    else if (event.deltaY > 0) {
        base.style.transform = `scale(${scale < 0.2? 0.2 : scale -= reScale})`
    }
}
// ************************** scroll direction

// ************************** disable scroll
// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };
let scale = 1;
const reScale = 0.1;

function preventDefault(e) {
    e.preventDefault();
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}
// modern Chrome requires { passive: false } when adding event
var supportsPassive = false;
try {
    window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
        get: function () { supportsPassive = true; }
    }));
} catch (e) { }

var wheelOpt = supportsPassive ? { passive: false } : false;
var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

// call this to Disable
function disableScroll() {
    // ************************** scroll direction
    window.addEventListener('wheel', onWheel);
    // ************************** scroll direction
    window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF
    window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
    window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile
    window.addEventListener('keydown', preventDefaultForScrollKeys, false);
}

// call this to Enable
function enableScroll() {
    // ************************** scroll direction
    window.removeEventListener('wheel', onWheel);
    // ************************** scroll direction

    window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.removeEventListener(wheelEvent, preventDefault, wheelOpt);
    window.removeEventListener('touchmove', preventDefault, wheelOpt);
    window.removeEventListener('keydown', preventDefaultForScrollKeys, false);
}
// ************************** disable scroll

document.addEventListener('DOMContentLoaded', function () {
    // ************************** dragscroll
    if (container) {
        let pos = { top: 0, left: 0, x: 0, y: 0 };
        var curDown;
        const mouseDownHandler = function (e) {
            // Change the cursor and prevent user from selecting the text
            container.style.cursor = 'grabbing';
            container.style.userSelect = 'none';
            pos = {
                // The current scroll 
                left: container.scrollLeft,
                top: container.scrollTop,
                // Get the current mouse position
                x: e.clientX,
                y: e.clientY,
            };
            curDown = true

            container.addEventListener('mousemove', mouseMoveHandler);
            container.addEventListener('mouseup', mouseUpHandler);
        };
        const mouseMoveHandler = function (e) {
            // How far the mouse has been moved
            if (curDown) {
                const dx = e.clientX - pos.x;
                const dy = e.clientY - pos.y;

                // Scroll the element
                container.scrollTop = pos.top - dy;
                container.scrollLeft = pos.left - dx;
            }
        };
        const mouseUpHandler = function () {
            container.style.cursor = 'grab';
            container.style.removeProperty('user-select');
            curDown = false
        };
        container.addEventListener('mousedown', mouseDownHandler);
    }
    // ************************** dragscroll

    // ********************** get event mouse move
    (function () {
        var mousePos;

        document.onmousemove = handleMouseMove;
        setInterval(getMousePosition, 100); // setInterval repeats every X ms

        function handleMouseMove(event) {
            var dot, eventDoc, doc, body, pageX, pageY;
            event = event || window.event; // IE-ism
            // If pageX/Y aren't available and clientX/Y are,
            // calculate pageX/Y - logic taken from jQuery.
            // (This is to support old IE)
            if (event.pageX == null && event.clientX != null) {
                eventDoc = (event.target && event.target.ownerDocument) || document;
                doc = eventDoc.documentElement;
                body = eventDoc.body;
                event.pageX = event.clientX +
                    (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
                    (doc && doc.clientLeft || body && body.clientLeft || 0);
                event.pageY = event.clientY +
                    (doc && doc.scrollTop || body && body.scrollTop || 0) -
                    (doc && doc.clientTop || body && body.clientTop || 0);
            }
            mousePos = {
                x: event.pageX,
                y: event.pageY
            };
        }
        function getMousePosition() {
            var pos = mousePos;
            if (!pos) {
            }
            else {
                if (container.matches(':hover')) {
                    disableScroll()
                }
                else {
                    enableScroll()
                }
            }
        }
        // ********************** get event mouse move

        // ********************** button zoom
        plus.addEventListener("click", function(){
            base.style.transform = `scale(${scale += reScale})`
        })
        minus.addEventListener("click", function(){
            base.style.transform = `scale(${scale < 0.2? 0.2 : scale -= reScale})`
        })
        // ********************** button zoom

        // ********************** hover chaining 
        targetHover.forEach(e => {
            e.addEventListener('mouseover', function(){
                targetHover.forEach(e => {
                    e.style.color = "red"
                })
            })
            e.addEventListener('mouseout', function(){
                targetHover.forEach(e => {
                    e.style.color = "black"
                })
            })
        })
        // ********************** hover chaining 
    })();
})

const t = "tes"