# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.0](https://gitlab.com/lordfahdan/dynamic-element/compare/v1.3.4...v2.0.0) (2021-08-16)


### ⚠ BREAKING CHANGES

* **index.html:** I broke everything

### Bug Fixes

* **index.html:** added new div ([030391f](https://gitlab.com/lordfahdan/dynamic-element/commit/030391f1be88d6a265960e9db2f1e89d6595ab04))

### [1.3.4](https://gitlab.com/lordfahdan/dynamic-element/compare/v1.3.0...v1.3.4) (2021-08-14)

### [1.3.3](https://gitlab.com/lordfahdan/dynamic-element/compare/v1.3.0...v1.3.3) (2021-08-14)

### [1.3.2](https://gitlab.com/lordfahdan/dynamic-element/compare/v1.3.0...v1.3.2) (2021-08-14)

### [1.3.1](https://gitlab.com/lordfahdan/dynamic-element/compare/v1.3.0...v1.3.1) (2021-08-14)

## [1.3.0](https://gitlab.com/lordfahdan/dynamic-element/compare/v1.2.0...v1.3.0) (2021-08-14)


### Features

* **index:** add new again div ([a92615f](https://gitlab.com/lordfahdan/dynamic-element/commit/a92615f4f088dc8980506b0436e7475659129cbf))
* **index:** add new span ([234806c](https://gitlab.com/lordfahdan/dynamic-element/commit/234806ca955e64166c68871e5fdeba0726301ebf))

## [1.2.0](https://gitlab.com/lordfahdan/dynamic-element/compare/v1.1.3...v1.2.0) (2021-08-14)


### Features

* **index:** add new div ([6cc0d38](https://gitlab.com/lordfahdan/dynamic-element/commit/6cc0d386c612e00bae2effdb07c54b44c3221845))

### [1.1.3](https://gitlab.com/lordfahdan/dynamic-element/compare/v1.1.2...v1.1.3) (2021-08-14)

### [1.1.2](https://gitlab.com/lordfahdan/dynamic-element/compare/v1.1.1...v1.1.2) (2021-08-14)


### Bug Fixes

* tambahan variable ([a562060](https://gitlab.com/lordfahdan/dynamic-element/commit/a5620607a48f14d763991fc0d8c2264fdc3a9d53))

### [1.1.1](https://gitlab.com/lordfahdan/dynamic-element/compare/v1.1.0...v1.1.1) (2021-08-14)


### Bug Fixes

* standard version fixing in package.json ([0736c46](https://gitlab.com/lordfahdan/dynamic-element/commit/0736c46abe99d1724f0b2029e31ee2c782b8a5de))

## 1.1.0 (2021-08-14)


### Features

* add package standard-version ([8006b2a](https://gitlab.com/lordfahdan/dynamic-element/commit/8006b2afe3807695aae736cd228edff003fa2a7e))
